#
#
#
function tpos(y0, yf, xf, returns)
{
	returns[0] = log(y0);
	returns[1] = (log(yf) - log(y0))/xf;
# print "tpos", y0, yf, xf, returns[0], returns[1];
}
function npos(y0, yf, x0, xf, returns)
{
	returns[1] = (log(y0) - log(yf))/(xf - x0);
	returns[0] = -(log(y0)+(returns[1]*x0));
# print "npos", y0, yf, xf, returns[0], returns[1];
}
BEGIN {
	FS=":";
}
{
	returnsa[0] = 0;
	returnsa[1] = 0;
	returnsb[0] = 0;
	returnsb[1] = 0;

	tpos($2,$3,$1, returnsa);
	npos($3,$4, $1, $5, returnsb);
	
	print $1 ":" returnsa[0] ":" returnsa[1] ":" returnsb[0] ":" returnsb[1];
}
