#
# $Id$
#
function conv(name)
{
	return _frag_[name];
}
BEGIN {
	n = 0;
	_frag_["suss"] = 0;
	_frag_["murm"] = 1;
	_frag_["armo"] = 2;
	_frag_["pigm"] = 3;
	_frag_["anti"] = 4;
	_frag_["scat"] = 5;
	_frag_["spra"] = 6;
	_frag_["legg"] = 7;
	_frag_["inte"] = 8;
	_frag_["rock"] = 9;
	_frag_["squa"] = 10;
	_frag_["belc"] = 11;
	_frag_["decl"] = 12;
}
/^#/ {
	next;
}
{
	print n++,conv($1);
}
# $Log$
