# $Id$
# run with gnuplot-loaded
#
set term pdfcairo 
set size 1,1
set noborder
plot "frags/antico.dat" title "antico" with l lw 5, \
	"frags/armonici.dat" title "armonici" with l lw 5, \
	"frags/belcanto.dat" title "belcanto" with l lw 5, \
	"frags/declam.dat" title "declam" with l lw 5, \
	"frags/interiez.dat" title "interiez" with l lw 5, \
	"frags/leggero.dat" title "leggero" with l lw 5, \
	"frags/murmuri.dat" title "murmuri" with l lw 5, \
	"frags/rock.dat" title "rock" with l lw 5, \
	"frags/scat.dat" title "scat" with l lw 5, \
	"frags/sprech.dat" title "sprech" with l lw 5, \
	"frags/squarcia.dat" title "squarcia" with l lw 5, \
	"frags/sussurri.dat" title "sussurri" with l lw 5
# $Log$
