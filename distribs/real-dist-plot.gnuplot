# $Id$
set term pdfcairo 
set size 1,1
set ytics ("sussurrato" 0, "mormorato" 1, "difonico" 2, "pigmeo" 3,\
	"medievale" 4, "scat" 5, "sprachgesang" 6, "crooning" 7,\
	"interiezioni" 8, "rock" 9, "a distesa" 10, "belcanto" 11,\
	"declamato" 12)
set noxzeroaxis
set noborder
set format y "%20s"
set ylabel "frammenti"
plot [-10:450][-4:18] "frags.database.out" title "frammenti" with steps, \
	 "frags.database.mean" title "media" with lines lw 5, \
	 "frags.database.sigma" title "scarto quadratico medio" with lines lw 5
# pause -1
# $Log$
