#
# $Id$
#
function conv(name)
{
	return _frag_[name];
}
function mean(n, num)
{
	_last_total_ += num;
	return _last_total_/(n+1);
}
function sigma(n, num, mean)
{
	_last_sigma_ += ((num-mean)^2);
	return sqrt(_last_sigma_/(n+1));
}
BEGIN {
	n = 0;
	_frag_["suss"] = 0;
	_frag_["murm"] = 1;
	_frag_["armo"] = 2;
	_frag_["pigm"] = 3;
	_frag_["anti"] = 4;
	_frag_["scat"] = 5;
	_frag_["spra"] = 6;
	_frag_["legg"] = 7;
	_frag_["inte"] = 8;
	_frag_["rock"] = 9;
	_frag_["squa"] = 10;
	_frag_["belc"] = 11;
	_frag_["decl"] = 12;
	_last_mean_ = 0;
}
/^#/ {
	next;
}
{
	num = conv($1);
	print n,sigma(n, num, mean(n, num));
	n++;
}
# $Log$
