#
#
#
function dcalc(x, gutter, a, b, c, d,
	result)
{
	result = 0;
	if (x < gutter)
		result = exp(a + (b * x));
	else
		result = exp(-(c + (d * x)));

	return result;
}
BEGIN {
	FS = ":";
}
{
	for (_x_ = 0; _x_ <= 408; ++_x_)
		print _x_, dcalc(_x_, $1, $2, $3, $4, $5);
}
