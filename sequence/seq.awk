#
# $Id$
#
function round(num)
{
	return int(num+0.5);
}
function calc_dur(start, end, x, nsegs)
{
	if (__a__ == 0)
	{
		__a__ = exp(start);
		__b__ = ((exp(end)) - __a__)/nsegs;
	}

	return log(__a__ + (__b__ * x));
}
function get_frag(short_name)
{
	return _short_frag_[short_name];
}
function get_color(frag)
{
	return _color[frag];
}
function print_dotted_line(pos, time, dur)
{
	++__n__;
	printf("Ld%d: line dotted from Top.sw + (%f,0) to Bot.nw + (%f,0)\n",
		__n__, pos, pos);
	printf("\"\\s-2%.2f\\s0\" at Ld%d.s - (0,.3)\n", time, __n__);
	printf("\"\\s-2(%.4f)\\s0\" at Ld%d.s - (0,.6)\n", dur, __n__);

	return __n__;
}
function print_frag(dur, frag,
	color, endpos, n)
{
	n = print_dotted_line(_x_coord_,_time_, dur);
	_time_ += dur;
	endpos = _x_coord_+(dur*scaling);
	printf("line from L%d.c + (%f,0) to L%d.c + (%f,0) # %s\n",
		frag, _x_coord_, frag, endpos, _color[_frags[frag]]);
	return (_x_coord_ = endpos);
}
function print_line_head(idx, spacing, frag)
{
	printf("L%d: \"%s\" at Top.sw - (0,%f) ljust\t# %s\n", idx, frag,
		(idx+1)*spacing, _color[frag]);
}
function print_header(f, spacing,
	i, bottom)
{
	print ".\\\" produced by seq.awk $Revision$";
	print ".PS";
	print "scale=2.54\nboxht=5.5";
	printf("Top: line invis from 0,0 to 16,0\n");
	for (i = 0; i < numfrags; ++i)
		print_line_head(i, spacing, f[i]);
	bottom = spacing*(i+1);
	printf("Bot: line invis from Top.sw - (0,%f) to Top.se - (0,%f)\n",
		bottom, bottom);
}
function page_turn()
{
	print ".SK";
}
function _print_separator()
{
	print ".PS\nscale=2.54";
	print "Start: line invis from 0,0 to 16,0";
	print "line thick 3 from Start.sw - (4,0.7) to Start.sw + (0,0)";
	print "line thick 3 from Start.sw - (4,0.9) to Start.sw + (0,-0.2)";
	print ".PE";
}
function print_separator()
{
	++___sepflag___;
	if (___sepflag___ % 2) 			# odd systems
		_print_separator();
	else
		page_turn();			# even systems
}
function print_system(idx, frag_input,
	dur, frag)
{
	dur = calc_dur(startlen, endlen, idx, numsegs);
	_totaldur_ += dur;
	frag = get_frag(frag_input);
	print_frag(dur, frag);
}
function print_trailer()
{
	print ".PE";
}
function reset_coords()
{
	_x_coord_ = _offset_;			# starting _offset_
}
BEGIN {
	startlen = 0.5590106
	endlen = 0.3455928
	numsegs = 402;
	numfrags = 13;
	spacing = 0.6;
	scaling=2.8;
	_offset_ = 2.6;
	_pagewid_ = 14;
	_time_ = 0;
	__idx__  = 0;
	reset_coords();

	_short_frag_["suss"] = 12;
	_short_frag_["murm"] = 11;
	_short_frag_["armo"] = 10;
	_short_frag_["pigm"] = 9;
	_short_frag_["anti"] = 8;
	_short_frag_["scat"] = 7;
	_short_frag_["spra"] = 6;
	_short_frag_["legg"] = 5;
	_short_frag_["inte"] = 4;
	_short_frag_["rock"] = 3;
	_short_frag_["squa"] = 2;
	_short_frag_["belc"] = 1;
	_short_frag_["decl"] = 0;

	_frags[12] = "sussurrato";	_color["sussurrato"] = "scarlet";
	_frags[11] = "murmurato";	_color["murmurato"] = "green";
	_frags[10] = "difonico";	_color["difonico"] = "magenta";
	_frags[9] = "pigmeo";		_color["pigmeo"] = "crimson";
	_frags[8] = "medioevale";	_color["medioevale"] = "red";
	_frags[7] = "scat";		_color["scat"] = "pink";
	_frags[6] = "sprachgesang";	_color["sprachgesang"] = "orange";
	_frags[5] = "leggero";		_color["leggero"] = "black";
	_frags[4] = "interiezioni";	_color["interiezioni"] = "yellow";
	_frags[3] = "rock";		_color["rock"] = "purple";
	_frags[2] = "a distesa";	_color["a distesa"] = "brown";
	_frags[1] = "belcantistico";	_color["belcantistico"] = "cyan";
	_frags[0] = "declamato";	_color["declamato"] = "blue";

	print_header(_frags, spacing);
}
END {
	dur = calc_dur(startlen, endlen, i, numsegs);
	print_dotted_line(_x_coord_, dur);
	print "# total duration = " _totaldur_;
	print_trailer();
}
/^#/ {
	next;
}
{
	if (_x_coord_ > _pagewid_)
	{
		dur = calc_dur(startlen, endlen, __idx__, numsegs);
		print_dotted_line(_x_coord_,_time_,dur);
		print_trailer();
		print_separator();
		reset_coords();
		print_header(_frags, spacing);
	}
	print_system(__idx__, $1);
	++__idx__;
}
#
# $Log$
#
